using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Library.Models
{
    public class Author
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        [Column(TypeName = "Date")]
        public DateTime? DateOfBirth { get; set; }

        [Column(TypeName = "Date")]
        public DateTime? DateOfDeath { get; set; }

        public IEnumerable<Book>? Books { get; set; }
    }
}
