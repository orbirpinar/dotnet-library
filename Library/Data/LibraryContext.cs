using Library.Models;
using Microsoft.EntityFrameworkCore;

namespace LocalLibrary.Data
{
    public class LibraryContext : DbContext
    {
        public LibraryContext(DbContextOptions<LibraryContext> options) : base(options)
        {
        }

        public DbSet<Author> Authors { get; set; }
        public DbSet<Book> Books { get; set; }
        public DbSet<BookInstance> BookInstances { get; set; }
        public DbSet<Genre> Genres { get; set; }
        public DbSet<Language> Languages { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<BookInstance>(entity =>
                {
                    entity.Property(e => e.LoanStatus)
                      .HasConversion(x => (int)x, x => (LoanStatus)x);
                }
          );
        }

    }
}
